# darknet4j (v.1.1)

Darknet Wrapper for Java

# Installation

1. Add the [darknet4j.jar](https://gitlab.com/intellisrc/darknet4j/raw/master/build/libs/darknet4j-1.1-linux-x86_64.jar?inline=false) in build/lib/ into your project
2. Download and compile [darknet](https://github.com/AlexeyAB/darknet)

# Versions

1.1 : Library is now using `AlexeyAB` darknet repository as it has several performance modifications (and other adventages). Updated up to Feb 2019.

1.0 : Library uses `pjreddie` (original) version. Updated up to Aug 2018.

# Usage

You need to specify in your project where the library is located, for example:

```java
System.setProperty("jna.library.path", "/home/user/project/lib/"); //Place the .so library in that path
```
    
# Example

The following class is an example of usage written in Groovy:

```java
package darknet.4j

import Box
import DarknetLibrary
import Detection
import Image
import Metadata
import Network
import com.sun.jna.Memory
import groovy.transform.CompileStatic

import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.IntBuffer

/**
 * @author A.Lepe
 */
@CompileStatic
class DarkNet {
    static class DetectedObj {
        String name
        int type
        float probability
        int x
        int y
        int w
        int h
    }

    final DarknetLibrary darknet
    final Network net
    final Metadata meta

    final static String darknetDir = System.getProperty("user.dir") + File.separator + "darknet/"

    DarkNet(File cfg, File weights, File metaFile) {
        System.setProperty("jna.library.path", darknetDir + "lib/")
        darknet = DarknetLibrary.INSTANCE
        net     = getNetwork(cfg, weights)
        meta    = getMetaData(metaFile)
    }

    static File getFile(String file) {
        File f = new File(darknetDir + file)
        if(!f.exists()) {
            println "File was not found: ${f.absolutePath}"
        }
        return f
    }

    Metadata getMetaData(File file) {
        return darknet.get_metadata(fileToByteBuffer(file))
    }

    Network getNetwork(File cfg, File weights) {
        return darknet.load_network(fileToByteBuffer(cfg), fileToByteBuffer(weights), 0)
    }

    static ByteBuffer fileToByteBuffer(File file) {
        assert file.exists(): "File : ${file.path} doesn't exists."
        println "Loading file: ${file.name}"
        Memory m = new Memory(1024)
        ByteBuffer buf = m.getByteBuffer(0, m.size()).order(ByteOrder.nativeOrder())
        buf.put(file.absolutePath.getBytes("UTF-8")).put((byte) 0).flip()
        return buf
    }

    List<DetectedObj> detect(String image) {
        return detect(getFile("data/" + image))
    }
    List<DetectedObj> detect(File image) {
        final float threshold = 0.5f
        final float hier_threshold = 0.5f
        final float nms = 0.45f

        Image im = darknet.load_image_color(fileToByteBuffer(image), 0, 0)
        println im.w + " x " + im.h

        darknet.network_predict_image(net, im)
        IntBuffer iBuff = IntBuffer.allocate(100)
        Detection dets = darknet.get_network_boxes(net, im.w, im.h, threshold, hier_threshold, null, 0, iBuff)
        int num = iBuff.get()
        if (nms) {
            darknet.do_nms_obj(dets, num, meta.classes, nms)
        }
        List<String> labels = []
        meta.names.pointer.getStringArray(0, meta.classes).each {
            String label ->
                labels << label
        }
        List<DetectedObj> res = []
        if(num) {
            dets.toArray(num).each {
                Detection d ->
                    Box b = d.bbox
                    int type = 0
                    d.prob.pointer.getFloatArray(0, meta.classes).each {
                        float f ->
                            if (f > 0) {
                                res << new DetectedObj(
                                        name: labels[type],
                                        type: type,
                                        probability: f,
                                        x: b.x as int,
                                        y: b.y as int,
                                        w: b.w as int,
                                        h: b.h as int
                                )
                            }
                            type++
                    }
    
            }
        }
        darknet.free_detections(dets, num)
        darknet.free_image(im)
        return res
    }
}
```
