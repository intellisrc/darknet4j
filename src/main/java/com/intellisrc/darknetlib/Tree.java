package com.intellisrc.darknetlib;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

import java.util.Arrays;
import java.util.List;

/**
 * This file was autogenerated by <a href="http://jnaerator.googlecode.com/">JNAerator</a>,<br>
 * a tool written by <a href="http://ochafik.com/">Olivier Chafik</a> that <a href="http://code.google.com/p/jnaerator/wiki/CreditsAndLicense">uses a few opensource projects.</a>.<br>
 * For help, please visit <a href="http://nativelibs4java.googlecode.com/">NativeLibs4Java</a> , <a href="http://rococoa.dev.java.net/">Rococoa</a>, or <a href="http://jna.dev.java.net/">JNA</a>.
 */
public class Tree extends Structure {
	/** C type : int* */
	public IntByReference leaf;
	public int n;
	/** C type : int* */
	public IntByReference parent;
	/** C type : int* */
	public IntByReference child;
	/** C type : int* */
	public IntByReference group;
	/** C type : char** */
	public PointerByReference name;
	public int groups;
	/** C type : int* */
	public IntByReference group_size;
	/** C type : int* */
	public IntByReference group_offset;
	public Tree() {
		super();
	}
	protected List<String> getFieldOrder() {
		return Arrays.asList("leaf", "n", "parent", "child", "group", "name", "groups", "group_size", "group_offset");
	}
	/**
	 * @param leaf C type : int*<br>
	 * @param parent C type : int*<br>
	 * @param child C type : int*<br>
	 * @param group C type : int*<br>
	 * @param name C type : char**<br>
	 * @param group_size C type : int*<br>
	 * @param group_offset C type : int*
	 */
	public Tree(IntByReference leaf, int n, IntByReference parent, IntByReference child, IntByReference group, PointerByReference name, int groups, IntByReference group_size, IntByReference group_offset) {
		super();
		this.leaf = leaf;
		this.n = n;
		this.parent = parent;
		this.child = child;
		this.group = group;
		this.name = name;
		this.groups = groups;
		this.group_size = group_size;
		this.group_offset = group_offset;
	}
	public Tree(Pointer peer) {
		super(peer);
	}
	public static class ByReference extends Tree implements Structure.ByReference {
		
	}
	public static class ByValue extends Tree implements Structure.ByValue {
		
	}
}
