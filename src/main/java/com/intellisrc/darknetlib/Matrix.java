package com.intellisrc.darknetlib;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.ptr.PointerByReference;

import java.util.Arrays;
import java.util.List;

/**
 * This file was autogenerated by <a href="http://jnaerator.googlecode.com/">JNAerator</a>,<br>
 * a tool written by <a href="http://ochafik.com/">Olivier Chafik</a> that <a href="http://code.google.com/p/jnaerator/wiki/CreditsAndLicense">uses a few opensource projects.</a>.<br>
 * For help, please visit <a href="http://nativelibs4java.googlecode.com/">NativeLibs4Java</a> , <a href="http://rococoa.dev.java.net/">Rococoa</a>, or <a href="http://jna.dev.java.net/">JNA</a>.
 */
public class Matrix extends Structure {
	public int rows;
	public int cols;
	/** C type : float** */
	public PointerByReference vals;
	public Matrix() {
		super();
	}
	protected List<String> getFieldOrder() {
		return Arrays.asList("rows", "cols", "vals");
	}
	/** @param vals C type : float** */
	public Matrix(int rows, int cols, PointerByReference vals) {
		super();
		this.rows = rows;
		this.cols = cols;
		this.vals = vals;
	}
	public Matrix(Pointer peer) {
		super(peer);
	}
	public static class ByReference extends Matrix implements Structure.ByReference {
		
	}
	public static class ByValue extends Matrix implements Structure.ByValue {
		
	}
}
