package com.intellisrc.darknetlib;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.ptr.FloatByReference;

import java.util.Arrays;
import java.util.List;

/**
 * This file was autogenerated by <a href="http://jnaerator.googlecode.com/">JNAerator</a>,<br>
 * a tool written by <a href="http://ochafik.com/">Olivier Chafik</a> that <a href="http://code.google.com/p/jnaerator/wiki/CreditsAndLicense">uses a few opensource projects.</a>.<br>
 * For help, please visit <a href="http://nativelibs4java.googlecode.com/">NativeLibs4Java</a> , <a href="http://rococoa.dev.java.net/">Rococoa</a>, or <a href="http://jna.dev.java.net/">JNA</a>.
 */
public class Image extends Structure {
	public int w;
	public int h;
	public int c;
	/** C type : float* */
	public FloatByReference data;
	public Image() {
		super();
	}
	protected List<String> getFieldOrder() {
		return Arrays.asList("w", "h", "c", "data");
	}
	/** @param data C type : float* */
	public Image(int w, int h, int c, FloatByReference data) {
		super();
		this.w = w;
		this.h = h;
		this.c = c;
		this.data = data;
	}
	public Image(Pointer peer) {
		super(peer);
	}
	public static class ByReference extends Image implements Structure.ByReference {
		
	}
	public static class ByValue extends Image implements Structure.ByValue {
		
	}
}
